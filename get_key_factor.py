import copy
import os
import sys

from utils import (export_flux_to_json, export_reaction_data, process_model, optimize_model, read_model, get_objective)

root_dir = os.path.abspath(os.path.join(os.path.abspath(__file__), os.pardir))
model_dir = os.path.join(root_dir, 'models')

model_dict = {model.split('.')[0]: model for model in os.listdir(model_dir)}

model_list = [file_name for file_name in os.listdir(model_dir)]

model = read_model(model_list[6])
reactions = model.reactions
reactions_dict = {reaction.id: reaction for reaction in reactions}

metabolites = model.metabolites
metabolites_dict = {metabolite.id: metabolite for metabolite in metabolites}

metabolite = 'accoa_c'

objective = get_objective(metabolites_dict[metabolite])

original_model = copy.deepcopy(model)
optimized_model = copy.deepcopy(model)


optimized_fluxes = optimize_model(original_model)
original_reactions = original_model.reactions

optimized_fluxes = optimize_model(optimized_model, objective)
optimized_reactions = optimized_model.reactions

overexpression_reactions = []
underexpressions_reactions = []

for i in range(len(reactions)):
    if reactions[i].id.split('_')[0] != 'BIOMASS' and reactions[i].id.split('_')[0] != 'EX':
        original_flux = abs(original_reactions[i].flux)
        optimized_flux = abs(optimized_reactions[i].flux)

        if optimized_flux > original_flux:
            overexpression_reactions.append(reactions[i].id)
        elif optimized_flux < original_flux:
            underexpressions_reactions.append(reactions[i].id)


export_flux_to_json(optimized_fluxes, model.id, metabolite)
print('The %s_reaction_data_%s.json has been saved!' % (model.id, metabolite))
print('Over-expression reactions: %s' % overexpression_reactions)
print('Under-expression reactions: %s' % underexpressions_reactions)

