import copy
import queue
import sys
import time

from sympy import maximum, minimum


class Node:
    def __init__(self, id, value) -> None:
        self.id = id
        self.parents = []
        self.children = []
        self.in_edges = []
        self.out_edges = []
        self.reactions = []
        self.value = value

    def add_child(self, child):
        self.children.append(child)

    def add_parent(self, parent):
        self.parents.append(parent)

    def del_child(self, child):
        self.children = [
            tmp_cihld for tmp_cihld in self.children if tmp_cihld != child]

    def del_parent(self, parent):
        self.parent = [
            tmp_parent for tmp_parent in self.parents if tmp_parent != parent]

    def add_in_edge(self, edge):
        self.in_edges.append(edge)

    def add_out_edge(self, edge):
        self.out_edges.append(edge)

    def del_in_edge(self, edge):
        self.in_edges = [
            tmp_edge for tmp_edge in self.in_edges if tmp_edge != edge]

    def del_out_edge(self, edge):
        self.out_edges = [
            tmp_edge for tmp_edge in self.out_edges if tmp_edge != edge]


class Edge:
    def __init__(self, src, dst, value=None) -> None:
        self.u = src
        self.v = dst
        self.flux = value['flux']
        self.maximum = value['maximum']
        self.minimum = value['minimum'] if 'minimum' in value else 0
        self.residual = value['maximum']
        self.reaction = value['reaction'] if 'reaction' in value else None

    def __add__(self, node):
        return self.flux + node.flux

    def __sub__(self, node):
        return self.flux - node.flux

    def set_flux(self, maximum, minimum, flux):
        self.maximum = maximum
        self.minimum = minimum
        self.flux = flux
        self.residual = maximum


class ModelGraph:
    def __init__(self, model, fluxes, cofactors) -> None:
        self.nodes = {}
        self.edges = {}
        self.init_graph(model, fluxes, cofactors)
        # self.set_s_t(source, target, cofactors)

    def add_node(self, node):
        if type(node) == str:
            if node not in self.nodes:
                self.nodes[node.id] = Node(node.id, node.value)
        else:
            if node.id not in self.nodes:
                self.nodes[node.id] = node

    def add_edge(self, u, v, value):
        edge_id = '-'.join((u.id, v.id))
        if edge_id not in self.edges:
            self.edges[edge_id] = Edge(u, v, value)

            u.add_child(v)
            v.add_parent(u)

            u.add_out_edge(self.edges[edge_id])
            v.add_in_edge(self.edges[edge_id])

    def del_edge(self, u, v):
        edge_id = '-'.join((u.id, v.id))
        if edge_id in self.edges:
            u.del_child(v)
            v.del_parent(u)
            del self.edges[edge_id]

    def init_graph(self, model, fluxes, cofactors):
        # print('Initializing molecular graph...')
        reactions = model.reactions
        for reaction in reactions:
            reaction_flux = fluxes[reaction.id]
            flux = round(reaction_flux[0], 8)
            minimum = round(reaction_flux[1], 8)
            maximum = round(reaction_flux[2], 8)
            if reaction.id.split('_')[0] != 'BIOMASS' and flux != 0:
                reactants = reaction.reactants
                products = reaction.products
                lower_bound = reaction.lower_bound
                upper_bound = reaction.upper_bound

                for reactant in reactants:
                    reactant_id = reactant.id
                    if reactant_id not in self.nodes:
                        self.nodes[reactant_id] = Node(reactant_id, reactant)
                    if len(products) > 0:
                        for product in products:
                            product_id = product.id
                            if product_id not in self.nodes:
                                self.nodes[product_id] = Node(
                                    product_id, product)

                            if reactant_id not in cofactors and product_id not in cofactors:
                                # Forward reactions
                                if flux > 0:
                                    # if lower_bound == 0 and upper_bound > 0:
                                    self.nodes[reactant_id].add_child(
                                        self.nodes[product_id])
                                    self.nodes[product_id].add_parent(
                                        self.nodes[reactant_id])

                                    edge_id = '-'.join((reactant_id,
                                                       product_id))
                                    if edge_id not in self.edges:
                                        value = {
                                            'flux': flux, 'maximum': maximum, 'minimum': 0, 'reaction': reaction}
                                        self.edges[edge_id] = Edge(
                                            self.nodes[reactant_id], self.nodes[product_id], value)

                                    self.nodes[reactant_id].add_out_edge(
                                        self.edges[edge_id])
                                    self.nodes[product_id].add_in_edge(
                                        self.edges[edge_id])

                                # Backward reactions
                                elif flux < 0:
                                    # elif lower_bound < 0 and upper_bound == 0:
                                    self.nodes[product_id].add_child(
                                        self.nodes[reactant_id])
                                    self.nodes[reactant_id].add_parent(
                                        self.nodes[product_id])

                                    edge_id = '-'.join((product_id,
                                                       reactant_id))
                                    if edge_id not in self.edges:
                                        value = {'flux': flux, 'maximum': abs(
                                            minimum), 'minimum': 0, 'reaction': reaction}
                                        self.edges[edge_id] = Edge(
                                            self.nodes[product_id], self.nodes[reactant_id], value)

                                    self.nodes[reactant_id].add_in_edge(
                                        self.edges[edge_id])
                                    self.nodes[product_id].add_out_edge(
                                        self.edges[edge_id])

                                # # Reversible reactions
                                # elif lower_bound < 0 and upper_bound > 0:
                                #     self.nodes[reactant_id].add_child(
                                #         self.nodes[product_id])
                                #     self.nodes[product_id].add_parent(
                                #         self.nodes[reactant_id])
                                #     edge_id = '-'.join((reactant_id,
                                #                        product_id))
                                #     if edge_id not in self.edges:
                                #         value = {
                                #             'flux': flux, 'maximum': maximum, 'minimum': 0, 'reaction': reaction}
                                #         self.edges[edge_id] = Edge(
                                #             self.nodes[reactant_id], self.nodes[product_id], value)

                                #     self.nodes[reactant_id].add_out_edge(
                                #         self.edges[edge_id])
                                #     self.nodes[product_id].add_in_edge(
                                #         self.edges[edge_id])

                                #     self.nodes[product_id].add_child(
                                #         self.nodes[reactant_id])
                                #     self.nodes[reactant_id].add_parent(
                                #         self.nodes[product_id])
                                #     edge_id = '-'.join((product_id,
                                #                        reactant_id))
                                #     if edge_id not in self.edges:
                                #         value = {'flux': flux, 'maximum': abs(
                                #             minimum), 'minimum': 0, 'reaction': reaction}
                                #         self.edges[edge_id] = Edge(
                                #             self.nodes[product_id], self.nodes[reactant_id], value)

                                #     self.nodes[reactant_id].add_in_edge(
                                #         self.edges[edge_id])
                                #     self.nodes[product_id].add_out_edge(
                                #         self.edges[edge_id])

                                    # if (minimum == 0 and maximum >= 0) or (minimum > 0 and maximum > 0):
                                    #     self.nodes[reactant_id].add_child(self.nodes[product_id])
                                    #     self.nodes[product_id].add_parent(self.nodes[reactant_id])

                                    #     edge_id = '-'.join((reactant_id, product_id))
                                    #     if edge_id not in self.edges:
                                    #         flux = {'flow': 0, 'capacity': abs(maximum-minimum), 'reaction':reaction}
                                    #         self.edges[edge_id] = Edge(self.nodes[reactant_id], self.nodes[product_id], flux)

                                    #     self.nodes[reactant_id].add_out_edge(self.edges[edge_id])
                                    #     self.nodes[product_id].add_in_edge(self.edges[edge_id])

                                    # if minimum<0 and maximum==0 or (minimum<0 and maximum<0):
                                    #     self.nodes[reactant_id].add_child(self.nodes[product_id])
                                    #     self.nodes[product_id].add_parent(self.nodes[reactant_id])

                                    #     edge_id = '-'.join((reactant_id, product_id))
                                    #     if edge_id not in self.edges:
                                    #         flux = {'flow': 0, 'capacity': abs(abs(minimum)-abs(maximum)), 'reaction':reaction}
                                    #         self.edges[edge_id] = Edge(self.nodes[reactant_id], self.nodes[product_id], flux)

                                    #     self.nodes[reactant_id].add_in_edge(self.edges[edge_id])
                                    #     self.nodes[product_id].add_out_edge(self.edges[edge_id])

                                    # if minimum < 0 and maximum > 0:
                                    #     self.nodes[reactant_id].add_child(self.nodes[product_id])
                                    #     self.nodes[product_id].add_parent(self.nodes[reactant_id])
                                    #     edge_id = '-'.join((reactant_id, product_id))
                                    #     if edge_id not in self.edges:
                                    #         flux = {'flow': 0, 'capacity': abs(maximum), 'reaction':reaction}
                                    #         self.edges[edge_id] = Edge(self.nodes[reactant_id], self.nodes[product_id], flux)

                                    #     self.nodes[reactant_id].add_out_edge(self.edges[edge_id])
                                    #     self.nodes[product_id].add_in_edge(self.edges[edge_id])

                                    #     self.nodes[product_id].add_child(self.nodes[reactant_id])
                                    #     self.nodes[reactant_id].add_parent(self.nodes[product_id])
                                    #     edge_id = '-'.join((product_id, reactant_id))
                                    #     if edge_id not in self.edges:
                                    #         flux = {'flow': 0, 'capacity': abs(minimum), 'reaction':reaction}
                                    #         self.edges[edge_id] = Edge(self.nodes[product_id], self.nodes[reactant_id], flux)

                                    #     self.nodes[reactant_id].add_in_edge(self.edges[edge_id])
                                    #     self.nodes[product_id].add_out_edge(self.edges[edge_id])

        # metabolites = model.metabolites
        # metabolites_dict = {
        #     metabolite.id: metabolite for metabolite in metabolites}
        # for metabolite in metabolites:
        #     metabolite_id = metabolite.id
        #     if metabolite_id not in cofactors:
        #         reactions = metabolite.reactions

        #         if metabolite_id not in self.nodes:
        #             self.nodes[metabolite_id] = Node(metabolite_id, metabolite)
        #         for reaction in reactions:
        #             products = reaction.products
        #             if len(products) > 0:
        #                 product_ids = [product.id for product in products]
        #                 for product_id in product_ids:
        #                     if product_id not in cofactors:
        #                         if product_id not in self.nodes:
        #                             self.nodes[product_id] = Node(
        #                                 product_id, metabolites_dict[product_id])

        #                         # flux  = round(reaction_flux['fluxes'], 8)
        #                         minimum = round(reaction_flux['minimum'], 8)
        #                         maximum = round(reaction_flux['maximum'], 8)
        #                         lower_bound = reaction.lower_bound
        #                         upper_bound = reaction.upper_bound

        #                         # Forward reactions
        #                         if lower_bound == 0 and upper_bound > 0:
        #                             self.nodes[metabolite_id].add_child(
        #                                 self.nodes[product_id])
        #                             self.nodes[product_id].add_parent(
        #                                 self.nodes[metabolite_id])
        #                             edge_id = '-'.join((metabolite_id,
        #                                                product_id))
        #                             if edge_id not in self.edges:
        #                                 flux = {'flow': 0, 'capacity': maximum}
        #                                 self.edges[edge_id] = Edge(
        #                                     self.nodes[metabolite_id], self.nodes[product_id], flux)

        #                         # Backward reactions
        #                         elif lower_bound < 0 and upper_bound == 0:
        #                             # flux = abs(flux)
        #                             tmp = copy.copy(minimum)
        #                             minimum = abs(maximum)
        #                             maximum = abs(tmp)

        #                             self.nodes[metabolite_id].add_child(
        #                                 self.nodes[product_id])
        #                             self.nodes[product_id].add_parent(
        #                                 self.nodes[metabolite_id])

                                # # Reversible reactions
                                # elif lower_bound < 0 and upper_bound > 0:
                                #     if minimum == 0 and maximum == 0:
                                #         for reactant in reactants:
                                #             for product in products:
                                #                 reactant_id = '.'.join(
                                #                     (reaction.id, reactant.id))
                                #                 product_id = '.'.join(
                                #                     (reaction.id, product.id))
                                #                 G.add_edges_from([(reactant_id, product_id)],
                                #                                 flow=fluxes,
                                #                                 capacity=abs(
                                #                                     maximum - minimum),
                                #                                 reaction=reaction)

                                #     if minimum == 0 and maximum > 0:
                                #         for reactant in reactants:
                                #             for product in products:
                                #                 reactant_id = '.'.join(
                                #                     (reaction.id, reactant.id))
                                #                 product_id = '.'.join(
                                #                     (reaction.id, product.id))
                                #                 G.add_edges_from([(reactant_id, product_id)],
                                #                                 flow=fluxes,
                                #                                 capacity=abs(
                                #                                     maximum - minimum),
                                #                                 reaction=reaction)

                                #     if minimum < 0 and maximum == 0:
                                #         fluxes = abs(fluxes)
                                #         tmp = copy.copy(minimum)
                                #         minimum = abs(maximum)
                                #         maximum = abs(tmp)
                                #         for reactant in reactants:
                                #             for product in products:
                                #                 reactant_id = '.'.join(
                                #                     (reaction.id, reactant.id))
                                #                 product_id = '.'.join(
                                #                     (reaction.id, product.id))
                                #                 G.add_edges_from([(product_id, reactant_id)],
                                #                                 flow=fluxes,
                                #                                 capacity=abs(maximum-minimum),
                                #                                 reaction=reaction)

                                #     if minimum < 0 and maximum > 0:
                                #         for reactant in reactants:
                                #             for product in products:
                                #                 reactant_id = '.'.join(
                                #                     (reaction.id, reactant.id))
                                #                 product_id = '.'.join(
                                #                     (reaction.id, product.id))
                                #                 G.add_edges_from([(reactant_id, product_id)],
                                #                                 flow=fluxes,
                                #                                 capacity=maximum,
                                #                                 reaction=reaction)
                                #                 G.add_edges_from([(product_id, reactant_id)],
                                #                                 flow=fluxes,
                                #                                 capacity=abs(minimum),
                                #                                 reaction=reaction)

                                #     if minimum > 0 and maximum> 0:
                                #         for reactant in reactants:
                                #             for product in products:
                                #                 reactant_id = '.'.join(
                                #                     (reaction.id, reactant.id))
                                #                 product_id = '.'.join(
                                #                     (reaction.id, product.id))
                                #                 G.add_edges_from([(reactant_id, product_id)],
                                #                                 flow=fluxes,
                                #                                 capacity=abs(maximum-minimum),
                                #                                 reaction=reaction)

                                #     if minimum < 0 and maximum < 0:
                                #         for reactant in reactants:
                                #             for product in products:
                                #                 reactant_id = '.'.join(
                                #                     (reaction.id, reactant.id))
                                #                 product_id = '.'.join(
                                #                     (reaction.id, product.id))
                                #                 G.add_edges_from([(product_id, reactant_id)],
                                #                                 flow=fluxes,
                                #                                 capacity=abs(
                                #                                     abs(minimum)-abs(maximum)),
                                #                                 reaction=reaction)

                                # edge_id = '-'.join((metabolite_id, product_id))
                                # if edge_id not in self.edges:
                                #     self.edges[edge_id] = Edge(self.nodes[metabolite_id], self.nodes[product_id], flux[reaction.id])
        # print('Molecular graph initialized.')

# class ModelGraph2:
#     def __init__(self, nodes, edges, cofactors) -> None:
#         self.nodes = {}
#         self.edges = {}
#         self.init_graph(nodes, edges, cofactors)
#         # self.set_s_t(source, target, cofactors)

#     def init_graph(self, nodes, edges, cofactors):
#         print('Initializing graph...')
#         for node_id in nodes:
#             if node_id.split('.')[1] not in cofactors:
#                 if node_id not in self.nodes:
#                     self.nodes[node_id] = Node(node_id, nodes[node_id])
#                 metabolite = nodes[node_id]
#                 reactions = metabolite['reactions']
#                 for reaction in reactions:
#                     products_id = [product.id for product in reaction.products]
#                     for product_id in products_id:
#                         combined_product_id = '.'.join(
#                             (reaction.id, product_id))
#                         if combined_product_id not in self.nodes:
#                             self.nodes[combined_product_id] = Node(
#                                 combined_product_id, nodes[combined_product_id])
#                         if product_id not in cofactors:
#                             self.nodes[node_id].add_child(
#                                 self.nodes[combined_product_id])
#                             self.nodes[combined_product_id].add_parent(
#                                 self.nodes[node_id])

#                             edge_id = '-'.join((node_id, combined_product_id))
#                             self.edges[edge_id] = Edge(
#                                 self.nodes[node_id], self.nodes[combined_product_id], edges[node_id][combined_product_id])

#                             self.nodes[node_id].add_out_edge(
#                                 self.edges[edge_id])
#                             self.nodes[combined_product_id].add_in_edge(
#                                 self.edges[edge_id])

#         for id in edges:
#             reactions = [reaction for reaction in nodes[id]['reactions']]
#             if id.split('.')[1] not in cofactors:
#                 if id not in self.nodes:
#                     self.nodes[id] = Node(id, nodes[id])

#                     # if id.split('.')[1] in source and id.split('.')[1] not in cofactors:
#                     #     self.source[id] = self.nodes[id]
#                     # if id.split('.')[1] in target and id.split('.')[1] not in cofactors:
#                     #     self.target[id] = self.nodes[id]

#                 dsts = edges[id]
#                 for dst in dsts:
#                     if dst.split('.')[1] not in cofactors:
#                         if dst not in self.nodes:
#                             self.nodes[dst] = Node(dst, nodes[dst])

#                             # if dst.split('.')[1] in source and dst.split('.')[1] not in cofactors:
#                             #     self.source[dst] = self.nodes[dst]
#                             # if dst.split('.')[1] in target and dst.split('.')[1] not in cofactors:
#                             #     self.target[dst] = self.nodes[dst]

#                         self.nodes[id].add_child(self.nodes[dst])
#                         self.nodes[dst].add_parent(self.nodes[id])

#                         edge_id = '-'.join((id, dst))
#                         self.edges[edge_id] = Edge(
#                             self.nodes[id], self.nodes[dst], dsts[dst])

#                         self.nodes[id].add_out_edge(self.edges[edge_id])
#                         self.nodes[dst].add_in_edge(self.edges[edge_id])

#         for id in nodes:
#             if id not in self.nodes:
#                 self.nodes[id] = Node(id, nodes[id])

#         print('Graph initialized.')

#     # def set_s_t(self, source, target, cofactors):

#     #     for s in source:
#     #         if s.split('.')[1] not in cofactors:
#     #             if s not in self.nodes:
#     #                 self.nodes[s] = Node(s, source[s])
#     #             self.source[s] = self.nodes[s]
#     #     for t in target:
#     #         if t.split('.')[1] not in cofactors:
#     #             if t not in self.nodes:
#     #                 self.nodes[t] = Node(t, target[t])
#     #             self.target[t] = self.nodes[t]


class Graph:
    def __init__(self, nodes={}, edges={}) -> None:
        self.nodes = {}
        self.edges = {}
        self.init_graph(nodes, edges)
        # self.set_s_t(source, target)

    def init_graph(self, nodes, edges):
        # print('Initializing graph...')
        for id in edges:
            if id not in self.nodes:
                self.nodes[id] = Node(id, nodes[id])

            dsts = edges[id]
            for dst in dsts:
                if dst not in self.nodes:
                    self.nodes[dst] = Node(dst, nodes[dst])

                self.nodes[id].add_child(self.nodes[dst])
                self.nodes[dst].add_parent(self.nodes[id])

                edge_id = '-'.join((str(id), str(dst)))
                self.edges[edge_id] = Edge(
                    self.nodes[id], self.nodes[dst], edges[id][dst])

                self.nodes[id].add_out_edge(self.edges[edge_id])
                self.nodes[dst].add_in_edge(self.edges[edge_id])

        for id in nodes:
            if id not in self.nodes:
                self.nodes[id] = Node(id, nodes[id])

        # print('Graph initialized.')

    def add_node(self, node):
        if node not in self.nodes:
            self.nodes[node.id] = node

    def add_edge(self, u, v, value):
        edge_id = '-'.join((u.id, v.id))
        if edge_id not in self.edges:
            flux = value['flux']
            capacity = value['capacity']
            value = {'flux': flux, 'capacity': capacity}
            self.edges[edge_id] = Edge(u, v, value)
            self.edges[edge_id].set_flux(value['capacity'], 0, value['flux'])

            u.add_child(v)
            v.add_parent(u)

            u.add_out_edge(self.edges[edge_id])
            v.add_in_edge(self.edges[edge_id])

    def del_edge(self, u, v):
        edge_id = '-'.join((u.id, v.id))
        if edge_id in self.edges:
            u.del_child(v)
            v.del_parent(u)

            u.del_out_edge(self.edges[edge_id])
            v.del_in_edge(self.edges[edge_id])

            del self.edges[edge_id]

    # def set_s_t(self, source, target):

    #     for s in source:
    #         if s not in self.nodes:
    #             self.nodes[s] = Node(s, source[s])
    #         self.source = self.nodes[s]
    #     for t in target:
    #         if t not in self.nodes:
    #             self.nodes[t] = Node(t, target[t])
    #         self.target = self.nodes[t]


class Solution:
    def __init__(self, graph, source, target) -> None:
        self.graph = graph

        self.s = []
        self.t = []
        self.maxflow = 0
        source, target = self.set_s_t(source, target)
        self.graph = copy.deepcopy(graph)

        self.source = self.graph.nodes[source]
        self.target = self.graph.nodes[target]

    def set_s_t(self, source, target):
        if len(source) == 1:
            source = source[0]
        elif len(source) > 1:
            value = {'flux': 0, 'capacity': 10}
            virtual_source = Node('virtual_source', value)
            self.graph.add_node(virtual_source)
            for s_id in source:
                self.graph.add_edge(
                    virtual_source, self.graph.nodes[s_id], value)
            source = virtual_source.id

        if len(target) == 1 or type(source) == str:
            target = target[0]
        elif len(target) > 1:
            value = {'flux': 0, 'capacity': 10}
            virtual_target = Node('virtual_target', value)
            self.graph.add_node(virtual_target)
            for t_id in target:
                self.graph.add_edge(
                    self.graph.nodes[t_id], virtual_target, value)
            target = virtual_target.id

        return source, target

    # Using BFS as a searching algorithm

    def searching_BFS(self, path):
        parent = {node: -1 for node in self.graph.nodes}
        queue = [self.source]
        visited = {node: False for node in self.graph.nodes}

        STATUS = False

        while queue:
            u = queue.pop(0)
            visited[u.id] = True
            children = []
            # children = [child for child in u.children]
            for child in u.children:
                edge = self.graph.edges['-'.join((u.id, child.id))]
                if child.id == self.target.id and edge.residual != 0:
                    visited[self.target.id] = True
                    parent[self.target.id] = u
                    node = self.target
                    while parent[node.id] != -1:
                        edge_id = '-'.join((parent[node.id].id, node.id))
                        edge = self.graph.edges[edge_id]
                        path[edge_id] = edge
                        node = parent[node.id]
                    STATUS = True
                    queue.clear()
                    break
                if visited[child.id] != True and edge.residual != 0:
                    queue.append(child)
                    parent[child.id] = u
                visited[child.id] = True
        return STATUS

    def get_blocking_edges(self, blocking_edges):
        level = {node: sys.maxsize for node in self.graph.nodes}

        STATUS = False

        queue = [self.source]
        level[self.source.id] = 0

        while queue:
            u = queue.pop(0)
            children = []
            if self.target.id in [child.id for child in u.children]:
                STATUS = True
            for child in u.children:
                edge_id = '-'.join((u.id, child.id))
                edge = self.graph.edges[edge_id]
                if edge.residual != 0 and level[child.id] > level[u.id]:
                    level[child.id] = level[u.id] + 1
                    blocking_edges[edge_id] = edge
                    children.append(child)
                queue.extend(children)

        return STATUS

    def get_shortest_path(self, path):
        visited = {node: False for node in self.graph.nodes}
        dist = {node: sys.maxsize for node in self.graph.nodes}
        parent = {node: 0 for node in self.graph.nodes}

        queue = [self.source]
        visited[self.source] = True
        dist[self.source.id] = 0

        while queue:
            u = queue.pop(0)
            children = []
            for child in u.children:
                edge = self.graph.edges['-'.join((u.id, child.id))]
                if edge.residual != 0 and visited[child.id] != True:
                    children.append(edge.v)
                visited[child.id] = True
            if self.target in children:
                visited[self.target.id] = True
                node = self.target
                while node != self.source:
                    edge_id = '-'.join((parent[node.id].id, node.id))
                    edge = self.graph.edges[edge_id]
                    path[edge_id] = edge
                    node = parent[node.id]
                break
            else:
                for child in children:
                    dist[child.id] = dist[u.id] + 1
                    parent[child.id] = u.id
                queue.extend(children)

    def get_maxflow(self):
        self.maxflow = sum([(edge.maximum - edge.residual)
                           for edge in self.graph.nodes[self.target.id].in_edges])
        return self.maxflow

    def get_path(self):
        tmp_path = {}
        self.searching_BFS(tmp_path)
        path_list = list(tmp_path)
        path_list.reverse()
        path = {p: tmp_path[p].reaction for p in path_list}
        return path

    def get_key_factors(self):
        key_factors = {}
        for s in self.s:
            for t in self.t:
                edge_id = '-'.join((s, t))
                if edge_id in self.graph.edges:
                    edge = self.graph.edges[edge_id]
                    key_factors[edge_id] = edge
        return key_factors

    def get_mincut(self):
        visited = {node: False for node in self.graph.nodes}
        dist = {node: sys.maxsize for node in self.graph.nodes}

        queue = [self.source]
        visited[self.source.id] = True
        dist[self.source.id] = 0

        self.s = [self.source.id]
        self.t = []

        while queue:
            u = queue.pop(0)
            children = []
            for child in u.children:
                edge = self.graph.edges['-'.join((u.id, child.id))]
                if edge.residual != 0 and visited[child.id] != True:
                    children.append(edge.v)
                    visited[child.id] = True
                    dist[child.id] = dist[u.id] + 1
                    self.s.append(child.id)
                    queue.extend(children)

        self.t = [node for node in self.graph.nodes if node not in self.s]
        return self.s, self.t

    # Applying Edmonds Karp algorithm

    def edmonds_karp(self):
        path = {}
        min_flow = sys.maxsize
        # print('Calculating solution...')
        while self.searching_BFS(path) and min_flow != 0:
            min_flow = min([path[edge].residual for edge in path])
            # print(min_flow)
            for edge in path:
                path[edge].residual -= min_flow
                # if path[edge].residual == 0:
                #     u = path[edge].u
                #     v = path[edge].v

                #     self.graph.del_edge(u, v)

            # Add reverse edges
            for edge in path:
                u = self.graph.nodes[edge.split('-')[1]]
                v = self.graph.nodes[edge.split('-')[0]]
                edge_id = '-'.join((u.id, v.id))
                if edge_id not in self.graph.edges:
                    value = {'flux': 0, 'capacity': min_flow}
                    reverse_edge = Edge(u, v)
                    reverse_edge.residual = min_flow
                    self.graph.edges[edge_id] = reverse_edge

                    u.add_child(v)
                    v.add_parent(u)

                    u.add_out_edge(self.graph.edges[edge_id])
                    v.add_in_edge(self.graph.edges[edge_id])

                else:
                    self.graph.edges[edge_id].residual += min_flow

            path = {}

        # print('Soultion calculated.')

    # Applying Dinic algorithm (Unfinished):
    def dinic(self):
        blocking_edges = {}
        while self.get_blocking_edges(blocking_edges):
            min_flow = min(
                [self.graph.edges[edge_id].residual for edge_id in blocking_edges])
            for edge in blocking_edges:
                blocking_edges[edge].residual -= min_flow
            # Add reverse edges
            for edge in blocking_edges:
                u = self.graph.nodes[edge.split('-')[1]]
                v = self.graph.nodes[edge.split('-')[0]]
                edge_id = '-'.join((u.id, v.id))
                if edge_id not in self.graph.edges:
                    value = {'flux': 0, 'capacity': min_flow}
                    reverse_edge = Edge(u, v, value)
                    reverse_edge.capacity = -1
                    self.graph.edges[edge_id] = reverse_edge

                    u.add_child(v)
                    v.add_parent(u)

                    u.add_out_edge(self.graph.edges[edge_id])
                    v.add_in_edge(self.graph.edges[edge_id])
                else:
                    self.graph.edges[edge_id].residual += min_flow

            blocking_edges = {}

        # print('Finish the calculation.')


class ModelSolution:
    def __init__(self, graph, source, target) -> None:

        self.s = []
        self.t = []
        self.maxflow = 0
        source, target = self.set_s_t(source, target)
        self.graph = graph

        self.source = self.graph.nodes[source]
        self.target = self.graph.nodes[target]

    def set_s_t(self, source, target):
        if len(source) == 1:
            source = source[0]
        elif len(source) > 1:
            value = {'flux': 0, 'maximum': 1000}
            virtual_source = Node('virtual_source', value)
            self.graph.add_node(virtual_source)
            for s_id in source:
                self.graph.add_edge(
                    virtual_source, self.graph.nodes[s_id], value)
                virtual_source.add_child(self.graph.nodes[s_id])
            source = virtual_source.id

        if len(target) == 1 or type(source) == str:
            target = target[0]
        elif len(target) > 1:
            value = {'flux': 0, 'maximum': 1000}
            virtual_target = Node('virtual_target', value)
            self.graph.add_node(virtual_target)
            for t_id in target:
                self.graph.add_edge(
                    self.graph.nodes[t_id], virtual_target, value)
                virtual_target.add_parent(self.graph.nodes[t_id])
            target = virtual_target.id

        return source, target

    # Using BFS as a searching algorithm
    def searching_BFS(self, path, flag=0):
        parent = {node: -1 for node in self.graph.nodes}
        queue = [self.source]
        visited = {node: False for node in self.graph.nodes}

        STATUS = False

        while queue:
            u = queue.pop(0)
            visited[u.id] = True
            for child in u.children:
                edge = self.graph.edges['-'.join((u.id, child.id))]
                if child.id == self.target.id and edge.residual != 0:
                    # if child.id == self.target.id:
                    visited[self.target.id] = True
                    parent[self.target.id] = u
                    node = self.target
                    while parent[node.id] != -1:
                        edge_id = '-'.join((parent[node.id].id, node.id))
                        edge = self.graph.edges[edge_id]
                        path[edge_id] = edge
                        node = parent[node.id]
                    STATUS = True
                    queue.clear()
                    break
                if edge.residual != 0 and visited[child.id] != True:
                    # if visited[child.id] != True:
                    queue.append(child)
                    parent[child.id] = u
                visited[child.id] = True
        return STATUS

    def get_shortest_path(self, path):
        visited = {node: False for node in self.graph.nodes}
        dist = {node: sys.maxsize for node in self.graph.nodes}
        parent = {node: 0 for node in self.graph.nodes}

        queue = [self.source]
        visited[self.source] = True
        dist[self.source.id] = 0

        while queue:
            u = queue.pop(0)
            children = []
            for child in u.children:
                edge = self.graph.edges['-'.join((u.id, child.id))]
                if edge.residual != 0 and visited[child.id] != True:
                    children.append(edge.v)
                visited[child.id] = True
            if self.target in children:
                visited[self.target.id] = True
                node = self.target
                while node != self.source:
                    edge_id = '-'.join((parent[node.id].id, node.id))
                    edge = self.graph.edges[edge_id]
                    path[edge_id] = edge
                    node = parent[node.id]
                break
            else:
                for child in children:
                    dist[child.id] = dist[u.id] + 1
                    parent[child.id] = u.id
                queue.extend(children)

    def get_maxflow(self):
        self.maxflow = sum([(edge.maximum - edge.residual)
                           for edge in self.target.in_edges])
        return self.maxflow

    def get_path(self):
        tmp_path = {}
        self.searching_BFS(tmp_path, flag=1)
        path_list = list(tmp_path)
        path_list.reverse()
        path = {p: tmp_path[p] for p in path_list}
        return path

    def get_key_factors(self):
        key_factors = {}
        for s in self.s:
            for t in self.t:
                edge_id = '-'.join((s, t))
                if edge_id in self.graph.edges:
                    edge = self.graph.edges[edge_id]
                    key_factors[edge_id] = edge
        return key_factors

    def get_mincut(self):
        visited = {node: False for node in self.graph.nodes}
        dist = {node: sys.maxsize for node in self.graph.nodes}

        queue = [self.source]
        visited[self.source.id] = True
        dist[self.source.id] = 0

        self.s = [self.source.id]

        while queue:
            u = queue.pop(0)
            for child in u.children:
                edge = self.graph.edges['-'.join((u.id, child.id))]
                if edge.residual != 0 and visited[child.id] != True:
                    visited[child.id] = True
                    dist[child.id] = dist[u.id] + 1
                    self.s.append(child.id)
                    queue.append(child)

        self.t = [node for node in self.graph.nodes if node not in self.s]
        return self.s, self.t

    # Applying Dinic's algorithm
    # def dinics(self):
    #     pass

    # Applying Edmonds Karp algorithm
    def edmonds_karp(self):
        start_time = time.time()
        path = {}
        min_flow = sys.maxsize
        # print('Calculating solution...')
        while self.searching_BFS(path) and min_flow != 0:
            min_flow = min([path[edge].residual for edge in path])
            for edge in path:
                path[edge].residual -= min_flow
                # if path[edge].residual == 0:
                #     u = path[edge].u
                #     v = path[edge].v
                #     self.graph.del_edge(u, v)
            # Add reverse edges
            for edge in path:
                u = self.graph.nodes[edge.split('-')[1]]
                v = self.graph.nodes[edge.split('-')[0]]
                edge_id = '-'.join((u.id, v.id))
                if edge_id not in self.graph.edges:
                    value = {'flux': 0, 'maximum': min_flow}
                    reverse_edge = Edge(u, v, value)
                    self.graph.edges[edge_id] = reverse_edge

                    u.add_child(v)
                    v.add_parent(u)

                    u.add_out_edge(self.graph.edges[edge_id])
                    v.add_in_edge(self.graph.edges[edge_id])

                else:
                    self.graph.edges[edge_id].residual += min_flow

            path = {}
        end_time = time.time()
        # print('Solution calculated. Calculation time: %f seconds.' %
        #       (end_time - start_time))


if __name__ == '__main__':
    nodes = {'s', 'v1', 'v2', 'v3', 'v4', 't'}
    nodes = {}
    nodes['s'] = 's'
    nodes['v1'] = 'v1'
    nodes['v2'] = 'v2'
    nodes['v3'] = 'v3'
    nodes['v4'] = 'v4'
    nodes['t'] = 't'

    edges = {}

    edges['s'] = {}
    edges['v1'] = {}
    edges['v2'] = {}
    edges['v3'] = {}
    edges['v4'] = {}
    edges['t'] = {}

    # # Graph 1
    # edges['s']['v1'] = {'flux': 0, 'maximum': 4}
    # edges['s']['v2'] = {'flux': 0, 'maximum': 2}
    # edges['v1']['v2'] = {'flux': 0, 'maximum': 1}
    # edges['v1']['v3'] = {'flux': 0, 'maximum': 2}
    # edges['v1']['v4'] = {'flux': 0, 'maximum': 4}
    # edges['v2']['v4'] = {'flux': 0, 'maximum': 2}
    # edges['v3']['t'] = {'flux': 0, 'maximum': 3}
    # edges['v4']['t'] = {'flux': 0, 'maximum': 3}

    # Graph 2
    edges['s']['v1'] = {'flux': 0, 'maximum': 10}
    edges['s']['v2'] = {'flux': 0, 'maximum': 10}
    edges['v1']['v2'] = {'flux': 0, 'maximum': 2}
    edges['v1']['v3'] = {'flux': 0, 'maximum': 4}
    edges['v1']['v4'] = {'flux': 0, 'maximum': 8}
    edges['v2']['v4'] = {'flux': 0, 'maximum': 9}
    edges['v3']['t'] = {'flux': 0, 'maximum': 10}
    edges['v4']['v3'] = {'flux': 0, 'maximum': 6}
    edges['v4']['t'] = {'flux': 0, 'maximum': 10}

    graph = Graph(nodes, edges)

    source = ['s']
    target = ['t']

    solution = Solution(graph, source, target)

    solution.edmonds_karp()

    max_flow = solution.get_maxflow()
    s, t = solution.get_mincut()
    key_factors = solution.get_key_factors()
    print('S cut is : %s' % s)
    print('T cut is : %s' % t)
    print('The maximum flow is: %s' % str(max_flow))
    print('The key factors are: %s' %
          ([str(key_factor) for key_factor in key_factors]))
