import numpy as np
from utils import vec2graph, graph2vec
from model import Graph

nodes = {'s', 'v1', 'v2', 'v3', 'v4', 't'}
nodes = {}
nodes['s'] = 's'
nodes['v1'] = 'v1'
nodes['v2'] = 'v2'
nodes['v3'] = 'v3'
nodes['v4'] = 'v4'
nodes['t'] = 't'

edges = {}
source = nodes['s']
target = nodes['t']

edges['s'] = {}
edges['v1'] = {}
edges['v2'] = {}
edges['v3'] = {}
edges['v4'] = {}
edges['t'] = {}

edges['s']['v1'] = {'flux': 0, 'capacity': 10}
edges['s']['v2'] = {'flux': 0, 'capacity': 10}
edges['v1']['v2'] = {'flux': 0, 'capacity': 2}
edges['v1']['v3'] = {'flux': 0, 'capacity': 4}
edges['v1']['v4'] = {'flux': 0, 'capacity': 8}
edges['v2']['v4'] = {'flux': 0, 'capacity': 9}
edges['v3']['t'] = {'flux': 0, 'capacity': 10}
edges['v4']['v3'] = {'flux': 0, 'capacity': 6}
edges['v4']['t'] = {'flux': 0, 'capacity': 10}

graph = Graph(nodes, edges)

print(graph)
vector_a = graph2vec(graph)
print(vector_a)