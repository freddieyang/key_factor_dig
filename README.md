# Metabolite pathway key factor digging

Metabolic pathway key factor digging project.

## dataprocess.py

A python file to process the data, such as reading the model.

## utils.py

A python file with some basic methods, such as reading the SBML models and processing the models.
