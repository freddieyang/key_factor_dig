import multiprocessing
import os
import pickle
import sys

import cobra
import networkx as nx
import pandas as pd
from tqdm import tqdm

from model import ModelGraph
from utils import process_model, read_model, save_model_data

sys.setrecursionlimit(10000)

root_dir = os.path.abspath(os.path.join(os.path.abspath(__file__), os.pardir))

model_dir = os.path.join(root_dir, 'models')
model_list = [os.path.join(model_dir, file_name)
              for file_name in os.listdir(model_dir)]
data_dir = os.path.join(root_dir, 'data')

cofactor_name = ['nad', 'nadh', 'coa', 'cob', 'com', 'f430', 'lpteichoic', 'LTA_MRSA', 'moco', 'pqq', 'amet', 'thmpp', 'tetdecacid', 'h', 'atp', 'adp', 'h2o', 'hco3', 'co2',
                 'pi', 'ppi', 'nadph', 'nadp', 'nh4', 'coa', 'amp', 'nh3', 'fmnh2', 'fmn', 'gdp', 'gtp', 'o2', 'fad', 'fadh2', 'acoa', 'h2o2', 'adocbi', 'ascb__L', 'btn']

compartments = ['c', 'e', 'p', 'm', 'x', 'r', 'v', 'n', 'g', 'u',
                'l', 'h', 'f', 's', 'im', 'cx', 'um', 'cm', 'i', 'mm', 'w', 'y']

cofactors = []
for ci in cofactor_name:
    for c in compartments:
        cofactors.append('_'.join((ci, c)))

if __name__ == '__main__':

    # Process the model.

    for model in tqdm(model_list):
        model = read_model(model)

        model, uptake_metabolites, secretion_metabolites, fluxes = process_model(model)

        model_graph = ModelGraph(model, fluxes, cofactors)
        data = {'model_id': model.id, 'model': model, 'model_graph': model_graph,
                'uptake_metabolites': uptake_metabolites, 'secretion_metabolites': secretion_metabolites, 'fluxes': fluxes}
        save_model_data(data)

    print('All models have been saved!')
