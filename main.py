import os
import pickle
import sys

import cobra
import pandas as pd
from tqdm import tqdm

from model import ModelGraph, ModelSolution
from utils import (export_reaction_data, process_model, read_model,
                   read_model_data)

sys.setrecursionlimit(10000)

root_dir = os.path.abspath(os.path.join(os.path.abspath(__file__), os.pardir))
model_dir = os.path.join(root_dir, 'models')

model_dict = {model.split('.')[0]:model for model in os.listdir(model_dir)}

model_list = [file_name for file_name in os.listdir(model_dir)]
# data_list = [os.path.join(data_dir, file_name) for file_name in os.listdir(data_dir)]


########################################################################################################################
# # 6: iJO1366
# # 58: core
model_data = read_model(model_list[58])
reactions = model_data.reactions
reaction_dict = {reaction.id:reaction for reaction in reactions}
metabolites = model_data.metabolites
metabolite_dict = {metabolite.id:metabolite for metabolite in metabolites}

cofactor_name = ['nad', 'nadh', 'coa', 'cob', 'com', 'f430', 'lpteichoic', 'LTA_MRSA', 'moco', 'pqq', 'amet', 'thmpp', 'tetdecacid', 'h', 'atp', 'adp', 'h2o', 'hco3', 'co2',
                'pi', 'ppi', 'nadph', 'nadp', 'nh4', 'coa', 'amp', 'nh3', 'fmnh2', 'fmn', 'gdp', 'gtp', 'o2', 'fad', 'fadh2', 'acoa', 'h2o2', 'adocbi', 'ascb__L', 'btn']

compartments = ['c', 'e', 'p', 'm', 'x', 'r', 'v', 'n', 'g', 'u',
                'l', 'h', 'f', 's', 'im', 'cx', 'um', 'cm', 'i', 'mm', 'w', 'y']

cofactors = []
for ci in cofactor_name:
    for c in compartments:
        cofactors.append('_'.join((ci, c)))

objective = reaction_dict['PDH']

model, uptake_metabolites, secretion_metabolites, fluxes = process_model(model_data, objective)

model_graph = ModelGraph(model, fluxes, cofactors)
########################################################################################################################


########################################################################################################################
# data = read_model_data(model_list[6])
# model = data['model']
# model_graph = data['model_graph']
# uptake_metabolites = data['uptake_metabolites']
# secretion_metabolites = data['secretion_metabolites']
# fluxes = data['fluxes']
########################################################################################################################


source = ['glc__D_e']
target = ['accoa_c']

solution = ModelSolution(model_graph, source, target)
path = solution.get_path()

export_reaction_data(path,'path_%s-%s'%(source[0],target[0]))


# The algorithm that used to optimize the object.
solution.edmonds_karp()

# Get the optimization results.
max_flow = solution.get_maxflow()
s,t = solution.get_mincut()
key_factors = solution.get_key_factors()

export_reaction_data(key_factors,'key_factors_%s'%(target[0]))

print('The synthesis pathway is: %s'%path)
print('The maximum flow is: %s'%str(max_flow))
print('The key factors are: %s'%({key_factors[key_factor].reaction:key_factors[key_factor].reaction for key_factor in key_factors}))
print('End!')
