import copy
import os
import pickle

import cobra
import networkx as nx
import numpy as np
import pandas as pd
from cobra.flux_analysis import flux_variability_analysis

from model import Edge, Graph

root_dir = os.path.abspath(os.path.join(os.path.abspath(__file__), os.pardir))

# model_list = [model for model in os.listdir(os.path.join(root_dir, 'models'))]

model_dir = os.path.join(root_dir, 'models')
data_dir = os.path.join(root_dir, 'data')

model_data_dir = os.path.join(data_dir, 'model_data')
reaction_data_dir = os.path.join(data_dir, 'reaction_data')


def process_model1(model, objective=None):
    """
    Process the model and save to a pickle file.
    """
    G = nx.DiGraph()
    model = cobra.io.read_sbml_model(os.path.join(model_dir, model))
    model_id = model.id
    # print('Reading %s finished!' % model_id)
    model.objective = objective
    solution = model.optimize()
    summary = model.summary()

    fba = solution.fluxes
    fva = flux_variability_analysis(model, model.reactions)
    flux = pd.concat([fba, fva], axis=1).T
    uptake_flux = summary.uptake_flux
    # uptake_metabolite = uptake_flux.metabolite
    secretion_flux = summary.secretion_flux
    # secretion_metabolite = secretion_flux.metabolite

    metabolites = model.metabolites
    metabolites_dict = {
        metabolite.id: metabolite for metabolite in model.metabolites}

    # reactions_dict = {reaction.id: reaction for reaction in model.reactions}

    # for metabolite in tqdm(metabolites):
    #     G.add_nodes_from([(metabolite, {'id': metabolite.id, 'name': metabolite.name, 'formula': metabolite.formula})])
    # print('All nodes have been addd into the graph!')
    forward_reactions = 0
    backward_reactions = 0
    reversible_reactions = 0
    for metabolite in model.metabolites:
        reactions = metabolite.reactions
        for reaction in reactions:
            reactants = reaction.reactants
            products = reaction.products
            if len(reactants) > 0 and len(products) > 0:
                reaction_flux = flux[reaction.id]
                fluxes = round(reaction_flux['fluxes'], 8)
                minimum = round(reaction_flux['minimum'], 8)
                maximum = round(reaction_flux['maximum'], 8)
                lower_bound = reaction.lower_bound
                upper_bound = reaction.upper_bound

                # Forward reactions
                if lower_bound == 0 and upper_bound > 0:
                    for reactant in reactants:
                        for product in products:
                            reactant_id = '.'.join((reaction.id, reactant.id))
                            product_id = '.'.join((reaction.id, product.id))
                            G.add_edges_from([(reactant_id, product_id)],
                                             flow=fluxes,
                                             capacity=abs(maximum),
                                             reaction=reaction)
                    forward_reactions += 1

                # Backward reactions
                elif lower_bound < 0 and upper_bound == 0:
                    fluxes = abs(fluxes)
                    for reactant in reactants:
                        for product in products:
                            reactant_id = '.'.join((reaction.id, reactant.id))
                            product_id = '.'.join((reaction.id, product.id))
                            G.add_edges_from([(product_id, reactant_id)],
                                             flow=fluxes,
                                             capacity=abs(minimum),
                                             reaction=reaction)
                    backward_reactions += 1

                # Reversible reactions
                elif lower_bound < 0 and upper_bound > 0:
                    if minimum == 0 and maximum == 0:
                        for reactant in reactants:
                            for product in products:
                                reactant_id = '.'.join(
                                    (reaction.id, reactant.id))
                                product_id = '.'.join(
                                    (reaction.id, product.id))
                                G.add_edges_from([(reactant_id, product_id)],
                                                 flow=fluxes,
                                                 capacity=abs(maximum),
                                                 reaction=reaction)

                    if minimum == 0 and maximum > 0:
                        for reactant in reactants:
                            for product in products:
                                reactant_id = '.'.join(
                                    (reaction.id, reactant.id))
                                product_id = '.'.join(
                                    (reaction.id, product.id))
                                G.add_edges_from([(reactant_id, product_id)],
                                                 flow=fluxes,
                                                 capacity=abs(maximum),
                                                 reaction=reaction)

                    if minimum < 0 and maximum == 0:
                        fluxes = abs(fluxes)
                        tmp = copy.copy(minimum)
                        minimum = abs(maximum)
                        maximum = abs(tmp)
                        for reactant in reactants:
                            for product in products:
                                reactant_id = '.'.join(
                                    (reaction.id, reactant.id))
                                product_id = '.'.join(
                                    (reaction.id, product.id))
                                G.add_edges_from([(product_id, reactant_id)],
                                                 flow=fluxes,
                                                 capacity=abs(maximum),
                                                 reaction=reaction)

                    if minimum < 0 and maximum > 0:
                        for reactant in reactants:
                            for product in products:
                                reactant_id = '.'.join(
                                    (reaction.id, reactant.id))
                                product_id = '.'.join(
                                    (reaction.id, product.id))
                                G.add_edges_from([(reactant_id, product_id)],
                                                 flow=fluxes,
                                                 capacity=abs(maximum),
                                                 reaction=reaction)
                                G.add_edges_from([(product_id, reactant_id)],
                                                 flow=fluxes,
                                                 capacity=abs(minimum),
                                                 reaction=reaction)

                    if minimum > 0 and maximum > 0:
                        for reactant in reactants:
                            for product in products:
                                reactant_id = '.'.join(
                                    (reaction.id, reactant.id))
                                product_id = '.'.join(
                                    (reaction.id, product.id))
                                G.add_edges_from([(reactant_id, product_id)],
                                                 flow=fluxes,
                                                 capacity=abs(maximum),
                                                 reaction=reaction)

                    if minimum < 0 and maximum < 0:
                        for reactant in reactants:
                            for product in products:
                                reactant_id = '.'.join(
                                    (reaction.id, reactant.id))
                                product_id = '.'.join(
                                    (reaction.id, product.id))
                                G.add_edges_from([(product_id, reactant_id)],
                                                 flow=fluxes,
                                                 capacity=abs(minimum),
                                                 reaction=reaction)

    # for reaction in reactions:
    #     reactants = reaction.reactants
    #     products = reaction.products
    #     if len(reactants) > 0 and len(products) > 0:
    #         reaction_flux = flux[reaction.id]
    #         fluxes = round(reaction_flux['fluxes'], 8)
    #         minimum = round(reaction_flux['minimum'], 8)
    #         maximum = round(reaction_flux['maximum'], 8)
    #         lower_bound = reaction.lower_bound
    #         upper_bound = reaction.upper_bound

    #         # Forward reactions
    #         if lower_bound == 0 and upper_bound > 0:
    #             for reactant in reactants:
    #                 for product in products:
    #                     reactant_id = '.'.join((reaction.id, reactant.id))
    #                     product_id = '.'.join((reaction.id, product.id))
    #                     G.add_edges_from([(reactant_id, product_id)],
    #                                      flow=fluxes,
    #                                      capacity=maximum-minimum,
    #                                      reaction=reaction)
    #             forward_reactions += 1

    #         # Backward reactions
    #         elif lower_bound < 0 and upper_bound == 0:
    #             fluxes = abs(fluxes)
    #             tmp = copy.copy(minimum)
    #             minimum = abs(maximum)
    #             maximum = abs(tmp)
    #             for reactant in reactants:
    #                 for product in products:
    #                     reactant_id = '.'.join((reaction.id, reactant.id))
    #                     product_id = '.'.join((reaction.id, product.id))
    #                     G.add_edges_from([(product_id, reactant_id)],
    #                                      flow=fluxes,
    #                                      capacity=maximum-minimum,
    #                                      reaction=reaction)
    #             backward_reactions += 1

    #         # Reversible reactions
    #         elif lower_bound < 0 and upper_bound > 0:
    #             if minimum == 0 and maximum == 0:
    #                 for reactant in reactants:
    #                     for product in products:
    #                         reactant_id = '.'.join((reaction.id, reactant.id))
    #                         product_id = '.'.join((reaction.id, product.id))
    #                         G.add_edges_from([(reactant_id, product_id)],
    #                                         flow=fluxes,
    #                                         capacity=maximum - minimum,
    #                                         reaction=reaction)

    #             if minimum == 0 and maximum>0:
    #                 for reactant in reactants:
    #                     for product in products:
    #                         reactant_id = '.'.join((reaction.id, reactant.id))
    #                         product_id = '.'.join((reaction.id, product.id))
    #                         G.add_edges_from([(reactant_id, product_id)],
    #                                         flow=fluxes,
    #                                         capacity=maximum - minimum,
    #                                         reaction=reaction)

    #             if minimum < 0 and maximum == 0:
    #                 fluxes = abs(fluxes)
    #                 tmp = copy.copy(minimum)
    #                 minimum = abs(maximum)
    #                 maximum = abs(tmp)
    #                 for reactant in reactants:
    #                     for product in products:
    #                         reactant_id = '.'.join((reaction.id, reactant.id))
    #                         product_id = '.'.join((reaction.id, product.id))
    #                         G.add_edges_from([(product_id, reactant_id)],
    #                                         flow=fluxes,
    #                                         capacity=maximum-minimum,
    #                                         reaction=reaction)

    #             if minimum < 0 and maximum > 0:
    #                 for reactant in reactants:
    #                     for product in products:
    #                         reactant_id = '.'.join((reaction.id, reactant.id))
    #                         product_id = '.'.join((reaction.id, product.id))
    #                         G.add_edges_from([(reactant_id, product_id)],
    #                                         flow=fluxes,
    #                                         capacity=maximum,
    #                                         reaction=reaction)
    #                         G.add_edges_from([(product_id, reactant_id)],
    #                                         flow=fluxes,
    #                                         capacity=abs(minimum),
    #                                         reaction=reaction)

    for node in G.nodes:
        metabolite = metabolites_dict[node.split('.')[1]]
        G.nodes[node]['metabolite'] = metabolite
        G.nodes[node]['id'] = metabolite.id
        G.nodes[node]['name'] = metabolite.name
        G.nodes[node]['formula'] = metabolite.formula
        G.nodes[node]['reactions'] = metabolite.reactions

    # print('All nodes have been addd into the graph!')

    data = {'model_id': model_id, 'model': model, 'flux': flux,
            'uptake_flux': uptake_flux, 'secretion_flux': secretion_flux, 'graph': G}
    # print('Model %s has been saved!' % model_id)
    return data


def save_model_data(model):
    """
    Save the model data into a pickle file.
    Parameters
    ----------
    model: the name of the model
    save_dir: the directory to save the pickle file

    Returns
    -------
    None

    """
    model_id = model['model_id']
    with open(os.path.join(model_data_dir, '%s.pkl' % model_id), 'wb') as f:
        pickle.dump(model, f)


def read_model_data(model):
    """
    Read the model pickle file.
    Parameters
    ----------
        model: the name of the model
    Returns
    -------
        data: the data of the model
    """
    model_name = '.'.join((model.split('.')[0], 'pkl'))
    data = pickle.load(open(os.path.join(model_data_dir, model_name), 'rb'))

    return data


def read_model(model_name):
    """
    Read the model sbml file.
    Parameters
    ----------
        model_name: the name of the model
    Returns:
    -------
        model: the model object
    """
    return cobra.io.read_sbml_model(os.path.join(model_dir, model_name))


def process_model(model, objective=None):
    """
    Process the model.
    Parameters
    ----------
        model: the name of the model
        objective: the objective reaction
    Returns
    -------
        model: the model object
        uptale_metabolites: the list of uptake metabolites
        secretion_metabolites: the list of secretion metabolites
        flux: the flux of the objective reaction
    """
    if objective != None:
        model.objective = objective
    solution = model.optimize()
    summary = model.summary()
    fba = solution.fluxes
    fva = flux_variability_analysis(model)
    fluxes = pd.concat([fba, fva], axis=1).T
    uptake_flux = summary.uptake_flux
    uptake_metabolite = uptake_flux.metabolite.values
    secretion_flux = summary.secretion_flux
    secretion_metabolite = secretion_flux.metabolite.values
    return model, uptake_metabolite, secretion_metabolite, fluxes


def graph2vec(graph):
    nodes_len = len(graph.nodes)
    vector = np.zeros((nodes_len, nodes_len))
    node_dict = {node_id: i for node_id,
                 i in zip(graph.nodes, range(nodes_len))}
    for edge_id in graph.edges:
        i, j = edge_id.split('-')
        vector[node_dict[i]][node_dict[j]] = graph.edges[edge_id].maximum
    return vector


def vec2graph(vector):
    nodes = {v: v for v in range(len(vector))}
    edges = {}
    for i in range(len(vector)):
        edges[i] = {}
        for j in range(len(vector)):
            if vector[i][j] != 0 and i != j:
                edges[i][j] = {'flux': 0, 'capacity': vector[i][j]}
    return Graph(nodes, edges)


def export_reaction_data(path, data_name):
    """
    Export the reaction data from the path into a json file.
    Parameters
    ----------
    reactions: the list of reactions
    data_dir: the directory to save the json file

    Returns
    -------
    None

    """
    reaction_data = {}
    for edge_id in path:
        edge = path[edge_id]
        reaction_id = edge.reaction.id
        model_id = edge.reaction.model.id
        reaction_data[reaction_id] = edge.maximum
    with open(os.path.join(reaction_data_dir, '%s_%s_reaction_data.json' % (model_id, data_name)), 'w') as f:
        str_reaction_data = str(reaction_data)
        str_reaction_data = str_reaction_data.replace('\'', '\"')
        f.write(str_reaction_data)

def optimize_model(model, objective = None):
    if objective != None:
        model.objective = objective
    solution = model.optimize()
    fluxes = solution.fluxes
    return fluxes


def export_flux_to_json(fluxes, model_id, objective):
    """
    Export the flux from the path into a json file.
    Parameters
    ----------
    reactions: the list of reactions
    data_dir: the directory to save the json file

    Returns
    -------
    None

    """
    axes = fluxes.axes[0]
    values = fluxes.values
    fluxes = {axes[i]: values[i] for i in range(len(fluxes))}
    with open(os.path.join(reaction_data_dir, '%s_reaction_data_%s.json' % (model_id, objective)), 'w') as f:
        str_flux = str(fluxes)
        str_flux = str_flux.replace('\'', '\"')
        f.write(str_flux)

def get_objective(metabolite = None):
    """
    Get the objective reaction of the model.
    Parameters
    ----------
    model: the model object
    Returns
    -------
    objective: the objective reaction
    """
    if metabolite != None:
        objective = []
        for reaction in metabolite.reactions:
            if metabolite in reaction.products:
                objective.append(reaction)
        return objective
    else:
        return 'biomass_reaction'