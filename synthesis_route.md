# MENAQUINONE-7 (MK7)

## Models: iYO844, iNF517, Recon3D, iYS854, iCN715, Recon3D

# 2-Hydroxyethanoic acid (乙醇酸)

## Models: 

# Phenazine-1-carboxylic acid (申嗪霉素, PCA)

## Models: 

# Ginsenoside F1 (人参皂苷F1)

## Metabolism: Saccharomyces cerevisiae (IMM904, iND750)

# Cytidine (胞苷) cytd_c

## Metabolism: Almost all

# Shikimic acid (莽草酸)

## Metabolism: Almost all
